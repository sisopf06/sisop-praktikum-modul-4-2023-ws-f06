#define FUSE_USE_VERSION 31
#define _XOPEN_SOURCE 700

#include <fuse.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <limits.h> 
#include <pwd.h>

// Path ke file log
const char *log_path = "/home/aida/fs_module.log";

// Fungsi untuk mencatat log
void log_event(const char *level, const char *cmd, const char *desc) {
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char timestamp[30];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm);

    char home_path[PATH_MAX];
    strcpy(home_path, getenv("HOME"));
    strcat(home_path, "/");
    strcat(home_path, log_path);

    if (access(home_path, F_OK) != 0) {
    fprintf(stderr, "File log tidak dapat dibuat.\n");
    return;
    }

    FILE *log_file = fopen(home_path, "a");

    fprintf(log_file, "%s::%s::%s::%s\n", level, timestamp, cmd, desc);
    fclose(log_file);
}

// Fungsi callback untuk membuat log pada operasi rmdir
static int log_rmdir(const char *path) {
    log_event("FLAG", "RMDIR", path);
    return 0;
}

// Fungsi callback untuk membuat log pada operasi unlink
static int log_unlink(const char *path) {
    log_event("FLAG", "UNLINK", path);
    return 0;
}

// Fungsi callback untuk membuat log pada operasi lainnya
static int log_report(const char *cmd, const char *path) {
    log_event("REPORT", cmd, path);
    return 0;
}

// Fungsi callback untuk membuat direktori modular
static int create_directory(const char *path, mode_t mode) {
    // Memeriksa apakah direktori yang dibuat memiliki awalan module_
    if (strncmp(path, "/module_", 8) == 0) {
        log_report("MKDIR", path);
        
        // Membuka direktori yang sedang diproses
        DIR *dir = opendir(path);
        if (dir == NULL) {
            return -errno;
        }
        
        // Membaca setiap entri di dalam direktori
        struct dirent *entry;
        while ((entry = readdir(dir)) != NULL) {
            // Mengabaikan entri "." dan ".."
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }
            
            // Mengkonstruksi path absolut untuk subdirektori
            char sub_path[PATH_MAX];
            snprintf(sub_path, PATH_MAX, "%s/%s", path, entry->d_name);
            
            // Memanggil fungsi create_directory secara rekursif pada subdirektori
            create_directory(sub_path, mode);
        }
        
        // Menutup direktori yang sedang diproses
        closedir(dir);
    } else {
        log_report("MKDIR", path);
    }

    return 0;
}

// Fungsi callback untuk membaca isi direktori
static int read_directory(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    (void) offset;
    (void) fi;

    DIR *dir;
    struct dirent *entry;

    dir = opendir(path);
    if (dir == NULL) {
        return -errno;
    }

    while ((entry = readdir(dir)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = entry->d_ino;
        st.st_mode = entry->d_type << 12;

        if (filler(buf, entry->d_name, &st, 0) != 0) {
            break;
        }
    }

    closedir(dir);
    return 0;
}

static struct fuse_operations operations = {
    .mkdir = create_directory,
    .rmdir = log_rmdir,
    .unlink = log_unlink,
    .readdir = read_directory,
};

int main(int argc, char *argv[]) {
    
    if (access("/home/aida", W_OK) != 0) {
    fprintf(stderr, "Tidak dapat mengakses direktori /home/aida.\n");
    return -1;
    }

    // Inisialisasi fuse
    umask(0);
    return fuse_main(argc, argv, &operations, NULL);
}
