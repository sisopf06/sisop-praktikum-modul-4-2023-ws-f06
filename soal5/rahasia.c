#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#define UNZIP_FOLDER "rahasia"

static const char * dirpath = "/home/plankton/Modul4/rahasia";
static int download_file() {
    char url[] = "https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes";
    char command[256];
    sprintf(command, "wget -O rahasia.zip '%s'", url);
    int status = system(command);
    if (status == -1) {
        printf("Gagal menjalankan perintah wget.\n");
        return 1;
    }
    printf("File berhasil diunduh.\n");
    return 0;
}
int adausername(const char * username) {
    FILE * file = fopen("regis.txt", "r");
    if (file == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }
    char line[202];
    while (fgets(line, sizeof(line), file) != NULL) {
        char * token = strtok(line, ";");
        if (token != NULL && strcmp(token, username) == 0) {
            fclose(file);
            return 1;
        }
    }
    fclose(file);
    return 0;
}
int regist(const char * username,
    const char * password) {
    FILE * file = fopen("regis.txt", "a");
    if (file == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }
    fprintf(file, "%s;%s\n", username, password);
    fclose(file);
    printf("Registrasi berhasil.\n");
    return 0;
}
void md5enkrip(const char * password,
    char * encryptedPassword) {
    char command[100];
    sprintf(command, "echo -n %s | md5sum | awk '{print $1}'", password);
    FILE * pipe = popen(command, "r");
    if (pipe == NULL) {
        printf("Error saat mengenkripsi password.\n");
        exit(1);
    }
    fgets(encryptedPassword, 100, pipe);
    // Menghapus karakter newline yang dihasilkan oleh md5sum
    encryptedPassword[strcspn(encryptedPassword, "\n")] = '\0';
    pclose(pipe);
}
int login(const char * username,
    const char * password) {
    FILE * file = fopen("regis.txt", "r");
    if (file == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }
    char line[100];
    while (fgets(line, sizeof(line), file) != NULL) {
        char * token = strtok(line, ";");
        if (token != NULL && strcmp(token, username) == 0) {
            char * savedPassword = strtok(NULL, ";");
            savedPassword[strcspn(savedPassword, "\n")] = '\0';
            if (savedPassword != NULL && strcmp(savedPassword, password) == 0) {
                fclose(file);
                return 1; // Berhasil login
            } else {
                fclose(file);
                return 0; // Password salah
            }
        }
    }
    fclose(file);
    return -1; // Username tidak ditemukan
}
static int rahasia_getattr(const char * path, struct stat * stbuf) {
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    res = lstat(fpath, stbuf);
    if (res == -1) return -errno;
    return 0;
}
static int rahasia_rename(const char * oldpath,
    const char * newpath) {
    char full_oldpath[1000];
    char full_newpath[1000];
    sprintf(full_oldpath, "%s%s", dirpath, oldpath);
    sprintf(full_newpath, "%s%s", dirpath, newpath);
    int result = rename(full_oldpath, full_newpath);
    if (result == -1) {
        perror("Gagal melakukan renaming");
    }
    return 0;
}
static int rahasia_readdir(const char * path, void * buf,
    fuse_fill_dir_t filler,
    off_t offset,
    struct fuse_file_info * fi) {
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else sprintf(fpath, "%s%s", dirpath, path);
    int res = 0;
    DIR * dp;
    struct dirent * de;
    (void) offset;
    (void) fi;
    dp = opendir(fpath);
    if (dp == NULL) return -errno;
    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        memset( & st, 0, sizeof(st));
        st.st_ino = de - > d_ino;
        st.st_mode = de - > d_type << 12;
        if (strstr(de - > d_name, "F06") == NULL) {
            if (S_ISDIR(st.st_mode) && strcmp(de - > d_name, ".") != 0 && strcmp(de - > d_name, "..") != 0) {
                char oldPath[2000];
                char newPath[2000];
                sprintf(oldPath, "%s/%s", fpath, de - > d_name);
                sprintf(newPath, "%s/%s_F06", fpath, de - > d_name);
                rename(oldPath, newPath);
                printf("%s\n", newPath);
            } else if (S_ISREG(st.st_mode)) {
                char oldPath[2000];
                char newPath[2000];
                sprintf(oldPath, "%s/%s", fpath, de - > d_name);
                sprintf(newPath, "%s/F06_%s", fpath, de - > d_name);
                rename(oldPath, newPath);
                printf("%s\n", newPath);
            }
        }
        res = (filler(buf, de - > d_name, & st, 0));
        if (res != 0) break;
    }
    closedir(dp);
    return 0;
}
static int rahasia_read(const char * path, char * buf,
    size_t size, off_t offset,
    struct fuse_file_info * fi) {
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else sprintf(fpath, "%s%s", dirpath, path);
    int res = 0;
    int fd = 0;
    (void) fi;
    fd = open(fpath, O_RDONLY);
    if (fd == -1) return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;
    close(fd);
    return res;
}
int menghitung() {
    FILE * resultFile, * extensionFile;
    char line[100];
    int folderCount = 0, jpgCount = 0, pngCount = 0, txtCount = 0, mp3Count = 0, docxCount = 0, gifCount = 0;
    system("touch result.txt");
    system("touch extension.txt");
    system("tree rahasia > result.txt");
    // Membuka file result.txt untuk dibaca
    resultFile = fopen("result.txt", "r");
    if (resultFile == NULL) {
        printf("Gagal membuka file result.txt.\n");
        return 1;
    }
    // Membaca setiap baris dari file result.txt
    while (fgets(line, sizeof(line), resultFile) != NULL) {
        // Menghapus karakter newline di akhir baris
        line[strcspn(line, "\n")] = '\0';
        // Memeriksa ekstensi dari line
        char * extension = strrchr(line, '.');
        char * folderCheck = strstr(line, "_F06");
        if (folderCheck != NULL) {
            folderCount++;
        } else if (extension != NULL) {
            // Memeriksa ekstensi yang valid
            if (strcmp(extension, ".jpg") == 0) {
                jpgCount++;
            } else if (strcmp(extension, ".png") == 0) {
                pngCount++;
            } else if (strcmp(extension, ".txt") == 0) {
                txtCount++;
            } else if (strcmp(extension, ".mp3") == 0) {
                mp3Count++;
            } else if (strcmp(extension, ".docx") == 0) {
                docxCount++;
            } else if (strcmp(extension, ".gif") == 0) {
                gifCount++;
            }
        }
    }
    // Menutup file result.txt setelah selesai dibaca
    fclose(resultFile);
    // Membuka file extension.txt untuk ditulis
    extensionFile = fopen("extension.txt", "w");
    if (extensionFile == NULL) {
        printf("Gagal membuka file extension.txt.\n");
        return 1;
    }
    // Menulis jumlah folder dan file dengan ekstensi yang valid ke dalam file extension.txt
    fprintf(extensionFile, "folder = %d\n", folderCount);
    fprintf(extensionFile, "jpg = %d\n", jpgCount);
    fprintf(extensionFile, "png = %d\n", pngCount);
    fprintf(extensionFile, "txt = %d\n", txtCount);
    fprintf(extensionFile, "mp3 = %d\n", mp3Count);
    fprintf(extensionFile, "docx = %d\n", docxCount);
    fprintf(extensionFile, "gif = %d\n", gifCount);
    // Menutup file extension.txt setelah selesai ditulis
    fclose(extensionFile);
    printf("Penghitungan selesai. Hasil disimpan dalam file extension.txt.\n");
    return 0;
}
static struct fuse_operations rahasia_oper = {
    .getattr = rahasia_getattr,
    .readdir = rahasia_readdir,
    .read = rahasia_read,
    .rename = rahasia_rename,
};
int main(int argc, char * argv[]) {
    char username[100];
    char password[100];
    char encryptedPassword[100];
    umask(0);
    if (access("rahasia.zip", F_OK) != 0) {
        download_file();
        system("unzip rahasia.zip");
    }
    pid_t pid = fork();
    if (pid == 0) {
        return fuse_main(argc, argv, & rahasia_oper, NULL);
    } else {
        system("docker pull ubuntu:focal");
        system("docker-compose up -d");
        menghitung();
        char opsi[10];
        printf("register / login\n");
        scanf("%s", opsi);
        if (strcmp(opsi, "register") == 0) {
            printf("Masukkan username: ");
            scanf("%s", username);
            username[strcspn(username, "\n")] = '\0';
            if (adausername(username)) {
                printf("Username sudah ada.\n");
            } else {
                printf("Masukkan password: ");
                scanf("%s", password);
                password[strcspn(password, "\n")] = '\0';
                md5enkrip(password, encryptedPassword);
                regist(username, encryptedPassword);
            }
        } else if (strcmp(opsi, "login") == 0) {
            printf("Masukkan username: ");
            scanf("%s", username);
            username[strcspn(username, "\n")] = '\0';
            printf("Masukkan password: ");
            scanf("%s", password);
            password[strcspn(password, "\n")] = '\0';
            md5enkrip(password, encryptedPassword);
            int loginResult = login(username, encryptedPassword);
            char tanya;
            if (loginResult == 1) {
                printf("Login berhasil.\n");
                while (1) {
                    printf("Ingin read isi directory? (Y/N)\n");
                    scanf(" %c", & tanya);
                    if (tanya == 'Y') {
                        system("docker exec -it rahasia_docker /bin/bash -c 'ls /usr/share'");
                    } else {
                        break;
                    }
                }
            } else if (loginResult == 0) {
                printf("Password salah.\n");
            } else {
                printf("Username tidak ditemukan.\n");
            }
        } else {
            printf("Penggunaan: ./run <folder> <register/login (optional)>\n");
            return 1;
        }
    }
}
