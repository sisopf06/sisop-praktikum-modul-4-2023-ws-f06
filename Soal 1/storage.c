#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(){
	system("kaggle datasets download -d bryanb/fifa-player-stats-database");
	sleep(5);
	system("unzip -q fifa-player-stats-database.zip");

	FILE *file = fopen("FIFA23_official_data.csv", "r");
	if (file == NULL){
		printf("Error opening file");
		exit(1);
	}
	FILE *output = fopen("Output_Storage.csv", "w");
	if (output == NULL){
		printf("Error printing output");
		exit(1);
	}

	char line[1000];

	while (fgets(line, 1000, file)){
		char name[100];
		int age;
		char club[100];
		char nationality[100];
		int potential;
		char photo[100];

		char *token = strtok(line, ",");
		strcpy(name, token);

		token = strtok(NULL, ",");
		age = atoi(token);

		token = strtok(NULL, ",");
		strcpy(club, token);

		token = strtok(NULL, ",");
		strcpy(nationality, token);

		token = strtok(NULL, ",");
		potential = atoi(token);

		token = strtok(NULL, ",");
		strcpy(photo, token);

		if(age < 25 && potential > 85 && strcmp(club, "Manchester City") != 0){
			fprintf(output, "Nama : %s\n", name);
			fprintf(output, "Umur : %d\n", age);
			fprintf(output, "Club : %s\n", club);
			fprintf(output, "Nationality : %s\n", nationality);
			fprintf(output, "Potensial : %d\n", potential);
			fprintf(output, "photo : %s\n", photo);
		}
	}
	fclose(file);
	fclose(output);

	return 0;
}
