#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(){
	system("kaggle datasets download -d bryanb/fifa-player-stats-database");
	sleep(5);
	system("unzip -q fifa-player-stats-database.zip");

	FILE *file = fopen("./FIFA23_official_data.csv", "r");
	if (file == NULL){
		printf("Error opening file");
		exit(1);
	}
	FILE *output = fopen("./Output_Storage.csv", "w");
	if (output == NULL){
		printf("Error printing file");
		exit(1);
	}

	char line[1000];
	fgets(line, 1000, file);

	while (fgets(line, 1000, file)){
		char name[100];
		int age;
		char club[100];
		char nationality[100];
		int potential;
		char photo[100];

		char *token = strtok(line, ",");

		for (int i = 1; i < 10; i++){
			if (i == 2)
				strcpy(name, token);

			else if (i == 3)
				age = atoi(token);

			else if (i == 9)
				strcpy(club, token);

			else if (i == 5)
				strcpy(nationality, token);

			else if (i == 8)
				potential = atoi(token);

			else if (i == 4)
				strcpy(photo, token);

			token = strtok(NULL, ",");

		}

		if(age < 25 && potential > 85 && strcmp(club, "Manchester City") != 0){
			fprintf(output, "Nama : %s,", name);
			fprintf(output, "Umur : %d,", age);
			fprintf(output, "Club : %s,", club);
			fprintf(output, "Nationality : %s,", nationality);
			fprintf(output, "Potensial : %d,", potential);
			fprintf(output, "photo : %s\n", photo);
		}

	}
	fclose(file);
	fclose(output);

	return 0;
}
