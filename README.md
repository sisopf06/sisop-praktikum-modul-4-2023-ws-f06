# sisop-praktikum-modul-4-2023-WS-F06

## 

## Soal Nomor 1
Deklarasi library
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
```
Mengunduh dataset dan unzip 
```
system("kaggle datasets download -d bryanb/fifa-player-stats-database");
sleep(5);
system("unzip -q fifa-player-stats-database.zip");
```
* Sleep digunakan agar unzip dan kaggle tidak berjalan secara bersamaan dan akan menunggu waktu untuk download dataset kaggle selesai terlebih dahulu.

Membuka source file dan mengekstrak yang diinginkan pada file output
```
FILE *file = fopen("./FIFA23_official_data.csv", "r");
if (file == NULL){
	printf("Error opening file");
	exit(1);
}
FILE *output = fopen("./Output_Storage.csv", "w");
if (output == NULL){
	printf("Error printing file");
	exit(1);
}

```
* Menggunakan fopen untuk membuka file pada direktori ./ atau home dengan nama tertentu
* fungsi w dan r tersedia pada FILE, r adalah read yaitu hanyauntuk membaca, dan w adalah write yaitu untuk menuliskan kepada file yang dituju

Mengekstrak file
```
char line[1000];

while (fgets(line, 1000, file)){
	char name[100];
	int age;
	char club[100];
	char nationality[100];
	int potential;
	char photo[100];

	char *token = strtok(line, ",");
	strcpy(name, token);

	token = strtok(NULL, ",");
	age = atoi(token);

	token = strtok(NULL, ",");
	strcpy(club, token);

	token = strtok(NULL, ",");
	strcpy(nationality, token);

	token = strtok(NULL, ",");
	potential = atoi(token);

	token = strtok(NULL, ",");
	strcpy(photo, token);

	if(age < 25 && potential > 85 && strcmp(club, "Manchester City") != 0){
		fprintf(output, "Nama : %s\n", name);
		fprintf(output, "Umur : %d\n", age);
		fprintf(output, "Club : %s\n", club);
		fprintf(output, "Nationality : %s\n", nationality);
		fprintf(output, "Potensial : %d\n", potential);
		fprintf(output, "photo : %s\n", photo);
	}
}

```
* char line merepresentasikan baris dari file yang dibuka
* selama membaca line dengan size 1000 pada file masih bernilai true, atau belum habis, maka loop akan terus berjalan
* mendeklarasikan nama, club, nasionality, dan photo sebagai char dan age serta potential sebagai integer
* token digunakan sebagai tempat menyimpan kata dari line(baris file yang saat ini sedang dibaca) yang berada dibelakang , dengan menggunakan strtok(variabel, penanda kata mana yang akan diambil sebelum tanda tersebut)
* strtok(NULL, penanda) akan mengambil lanjutan dari kata yang telah diambil sebelumnya dalam variabel yang sama, sehingga hanya perlu melanjutkan hingga dijumpai penanda pada token
* pada file csv, kolom dapat dituliskan sebagai ","
* untuk char, pengambilan menggunakan string copy(nama variabel, nama variabel yang akan dicopy pada variabel pertama)
* untuk integer, digunakan atoi, yaitu untuk mengubah string dengan integer value kedalam integer

Pada file tersebut, terjadi kesalahan dan error pada saat pengambilan data. Hal ini dikarenakan tidak adanya indeks kata ke berapa yang akan diambil pada strtok dan juga masih terbacanya header dari file, sehingga

Refisi:
```
char line[1000];
fgets(line, 1000, file);

while (fgets(line, 1000, file)){
	char name[100];
	int age;
	char club[100];
	char nationality[100];
	int potential;
	char photo[100];

	char *token = strtok(line, ",");

	for (int i = 1; i < 10; i++){
		if (i == 2)
			strcpy(name, token);

		else if (i == 3)
			age = atoi(token);

		else if (i == 9)
			strcpy(club, token);

		else if (i == 5)
			strcpy(nationality, token);

		else if (i == 8)
			potential = atoi(token);

		else if (i == 4)
			strcpy(photo, token);

		token = strtok(NULL, ",");

	}

	if(age < 25 && potential > 85 && strcmp(club, "Manchester City") != 0){
		fprintf(output, "Nama : %s,", name);
		fprintf(output, "Umur : %d,", age);
		fprintf(output, "Club : %s,", club);
		fprintf(output, "Nationality : %s,", nationality);
		fprintf(output, "Potensial : %d,", potential);
		fprintf(output, "photo : %s\n", photo);
	}

```
* fgets pertama akan membaca header dari file
* pada setiap line, pertama akan diambil kata dari strtok sebelum tanda ","
* nama berada pada kolom ke-2, umur berada pada kolom ke-3, club berada pada kolom ke-9, nasionality berada pada kolom ke-5, potensial pada kolom ke-8, dan foto pada kolom ke-4
* pada setiap kolom setelah pengambilan dengan penanda "," maka akan dilakukan strtok(NULL, ",") untuk melanjutkan dari pengambilan sebelumnya hingga kolom ke-9
* strcmp atau string compare akan mengembalikan nilai 0 apabila yang dibandingkan sama persis, sehingga apabila != 0 maka akan menandakan bahwa pemain bukan berasal dari club Manchester United

Penutupan file
```
fclose(file);
fclose(output);

```


## Soal Nomor 4
Deklarasikan semua library yang dibutuhkan
```
#define FUSE_USE_VERSION 31
#define _XOPEN_SOURCE 700

#include <fuse.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <limits.h> 
#include <pwd.h>
```
- `define FUSE_USE_VERSION 31` digunakan untuk menunjukkan program menggunakan FUSE API versi 31
- `define _XOPEN_SOURCE 700` digunakan untuk mengaktifkan fitur dan menunjukkan bahwa fitur dengan level XSI versi 700 diaktifkan

**Menuju path ke file log** 

`const char *log_path = "/home/aida/fs_module.log";` 
- Nantinya akan terbentuk file log pada path tersebut 

**Fungsi log_event**
```
void log_event(const char *level, const char *cmd, const char *desc) {
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char timestamp[30];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm);

    char home_path[PATH_MAX];
    strcpy(home_path, getenv("HOME"));
    strcat(home_path, "/");
    strcat(home_path, log_path);

    if (access(home_path, F_OK) != 0) {
    fprintf(stderr, "File log tidak dapat dibuat.\n");
    return;
    }

    FILE *log_file = fopen(home_path, "a");

    fprintf(log_file, "%s::%s::%s::%s\n", level, timestamp, cmd, desc);
    fclose(log_file);
}
```
- `void log_event(const char *level, const char *cmd, const char *desc)`digunakan untuk mencatat log. Fungsi ini menerima tiga argumen: level (tingkat log), cmd (perintah), dan desc (deskripsi).

- `time_t t = time(NULL)`. Variabel t bertipe time_t digunakan untuk menyimpan waktu saat ini dalam bentuk UNIX timestamp. Fungsi time(NULL) mengembalikan waktu saat ini dalam detik.

- `struct tm *tm = localtime(&t)`. Struct tm digunakan untuk menyimpan informasi waktu seperti tahun, bulan, hari, jam, menit, dan detik. Fungsi localtime(&t) mengonversi time_t ke waktu lokal (berdasarkan konfigurasi sistem).

- `char timestamp[30]`. Array timestamp digunakan untuk menyimpan waktu dalam bentuk string.

- `strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm)`. Fungsi strftime digunakan untuk mengonversi waktu dalam bentuk struct tm menjadi string dengan format yang ditentukan. Format "%y%m%d-%H:%M:%S" akan menghasilkan string dengan format tahun (2 digit), bulan, hari, jam, menit, dan detik.

- `char home_path[PATH_MAX]`. Array home_path digunakan untuk menyimpan path lengkap ke file log.

- `strcpy(home_path, getenv("HOME"))`. Fungsi getenv("HOME") digunakan untuk mendapatkan path ke direktori home pengguna dari environment. Nilai tersebut akan disalin ke home_path menggunakan strcpy.

- `strcat(home_path, "/")`. String "/" ditambahkan ke home_path menggunakan strcat untuk memisahkan path direktori dengan nama file log.

- `strcat(home_path, log_path)`. String log_path ditambahkan ke home_path menggunakan strcat untuk menyertakan nama file log.

- `if (access(home_path, F_OK) != 0)`. Fungsi access digunakan untuk memeriksa aksesibilitas file dengan path yang diberikan. F_OK adalah konstanta yang menunjukkan pengecekan keberadaan file. Jika access mengembalikan nilai bukan 0, berarti file tidak dapat diakses atau tidak ada. Pada kode ini, kita memeriksa apakah file log sudah ada.

- `fprintf(stderr, "File log tidak dapat dibuat.\n")`. Jika file log tidak dapat diakses atau tidak ada, pesan kesalahan akan dicetak ke stderr.

- `FILE *log_file = fopen(home_path, "a")`. Fungsi fopen digunakan untuk membuka file log dengan mode "a" (append), yang berarti data baru akan ditambahkan ke akhir file yang sudah ada (jika ada), atau file akan dibuat jika belum ada.

- `fprintf(log_file, "%s::%s::%s::%s\n", level, timestamp, cmd, desc)`. Fungsi fprintf digunakan untuk mencetak string format ke file log yang sudah dibuka. Format %s::%s::%s::%s\n mencetak tingkat log, timestamp, perintah, dan deskripsi dengan pemisah "::".

- `fclose(log_file)`. Fungsi fclose digunakan untuk menutup file log setelah selesai menulis.

**Fungsi log_rmdir**
```
static int log_rmdir(const char *path) {
    log_event("FLAG", "RMDIR", path);
    return 0;
}
```
- `static int log_rmdir(const char *path)`. Ini adalah deklarasi fungsi log_rmdir. Fungsi ini menerima satu argumen yaitu path yang merupakan path lengkap dari direktori yang akan dihapus.

- `log_event("FLAG", "RMDIR", path)`. Pada fungsi log_rmdir, kita memanggil fungsi log_event dengan tiga argumen: level ("FLAG"), cmd ("RMDIR"), dan desc (nilai path). Ini berarti kita mencatat log dengan tingkat "FLAG", perintah "RMDIR", dan deskripsi berupa path direktori yang dihapus.

- `return 0`. Fungsi log_rmdir mengembalikan nilai 0. Nilai kembalian ini menunjukkan bahwa operasi rmdir berjalan dengan sukses.

**Fungsi log_unlink**
```
static int log_unlink(const char *path) {
    log_event("FLAG", "UNLINK", path);
    return 0;
}
```
- `static int log_unlink(const char *path)`. Ini adalah deklarasi fungsi log_unlink. Fungsi ini menerima satu argumen yaitu path yang merupakan path lengkap dari file yang akan dihapus.

- `log_event("FLAG", "UNLINK", path)`. Pada fungsi log_unlink, kita memanggil fungsi log_event dengan tiga argumen: level ("FLAG"), cmd ("UNLINK"), dan desc (nilai path). Ini berarti kita mencatat log dengan tingkat "FLAG", perintah "UNLINK", dan deskripsi berupa path file yang dihapus.

- `return 0`. Fungsi log_unlink mengembalikan nilai 0. Nilai kembalian ini menunjukkan bahwa operasi unlink berjalan dengan sukses.

**Fungsi log_report**
```
static int log_report(const char *cmd, const char *path) {
    log_event("REPORT", cmd, path);
    return 0;
}
```
- `static int log_report(const char *cmd, const char *path)`. Ini adalah deklarasi fungsi log_report. Fungsi ini menerima dua argumen yaitu cmd yang merupakan perintah dari operasi yang sedang dilakukan dan path yang merupakan path dari objek yang terlibat dalam operasi tersebut.

- `log_event("REPORT", cmd, path)`. Pada fungsi log_report, kita memanggil fungsi log_event dengan tiga argumen: level ("REPORT"), cmd (nilai cmd), dan desc (nilai path). Ini berarti kita mencatat log dengan tingkat "REPORT", perintah sesuai dengan nilai cmd, dan deskripsi sesuai dengan nilai path.

- `return 0`. Fungsi log_report mengembalikan nilai 0. Nilai kembalian ini menunjukkan bahwa operasi telah berhasil.

**Fungsi create_directory**
```
static int create_directory(const char *path, mode_t mode) {
    // Memeriksa apakah direktori yang dibuat memiliki awalan module_
    if (strncmp(path, "/module_", 8) == 0) {
        log_report("MKDIR", path);
        
        // Membuka direktori yang sedang diproses
        DIR *dir = opendir(path);
        if (dir == NULL) {
            return -errno;
        }
        
        // Membaca setiap entri di dalam direktori
        struct dirent *entry;
        while ((entry = readdir(dir)) != NULL) {
            // Mengabaikan entri "." dan ".."
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }
            
            // Mengkonstruksi path absolut untuk subdirektori
            char sub_path[PATH_MAX];
            snprintf(sub_path, PATH_MAX, "%s/%s", path, entry->d_name);
            
            // Memanggil fungsi create_directory secara rekursif pada subdirektori
            create_directory(sub_path, mode);
        }
        
        // Menutup direktori yang sedang diproses
        closedir(dir);
    } else {
        log_report("MKDIR", path);
    }

    return 0;
}
```
- `static int create_directory(const char *path, mode_t mode)`. Ini adalah deklarasi fungsi create_directory. Fungsi ini menerima dua argumen yaitu path yang merupakan path dari direktori yang akan dibuat, dan mode yang merupakan mode permissions dari direktori.

- `if (strncmp(path, "/module_", 8) == 0)`. Pada blok ini, kita memeriksa apakah direktori yang akan dibuat memiliki awalan "module_". Jika iya, maka kita akan melakukan modularisasi pada direktori tersebut.

- `log_report("MKDIR", path)`. Kita memanggil fungsi log_report dengan tingkat "REPORT" dan perintah "MKDIR" untuk mencatat operasi pembuatan direktori ke dalam log.

- `DIR *dir = opendir(path)`. Kita membuka direktori yang sedang diproses menggunakan fungsi opendir. Jika direktori tidak dapat dibuka, kita mengembalikan kode error menggunakan -errno.

- `struct dirent *entry; while ((entry = readdir(dir)) != NULL) { ... }`. Kita membaca setiap entri di dalam direktori yang sedang diproses menggunakan fungsi readdir. Selama masih terdapat entri yang belum dibaca, kita akan melakukan iterasi.

- `if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) { continue; }`. Kita mengabaikan entri "." dan ".." dalam direktori, karena ini adalah entri yang merujuk pada direktori itu sendiri dan direktori induk.

- `char sub_path[PATH_MAX]; snprintf(sub_path, PATH_MAX, "%s/%s", path, entry->d_name)`. Kita mengkonstruksi path absolut untuk setiap subdirektori di dalam direktori yang sedang diproses.

- `create_directory(sub_path, mode)`. Kita memanggil fungsi create_directory secara rekursif pada setiap subdirektori yang ditemukan. Hal ini dilakukan agar modularisasi juga berlaku untuk subdirektori di dalam direktori yang sedang diproses.

- `closedir(dir)`. Setelah selesai membaca semua entri di dalam direktori, kita menutup direktori yang sedang diproses menggunakan fungsi closedir.

- `else { log_report("MKDIR", path); }`. Jika direktori yang akan dibuat tidak memiliki awalan "module_", kita tetap mencatat operasi pembuatan direktori ke dalam log menggunakan fungsi log_report.

- `return 0`. Fungsi create_directory mengembalikan nilai 0. Nilai kembalian ini menunjukkan bahwa operasi telah berhasil.

**Fungsi read_directory**
```
static int read_directory(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    (void) offset;
    (void) fi;

    DIR *dir;
    struct dirent *entry;

    dir = opendir(path);
    if (dir == NULL) {
        return -errno;
    }

    while ((entry = readdir(dir)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = entry->d_ino;
        st.st_mode = entry->d_type << 12;

        if (filler(buf, entry->d_name, &st, 0) != 0) {
            break;
        }
    }

    closedir(dir);
    return 0;
}
```
- `(void) offset; dan (void) fi`. Baris ini digunakan untuk menghilangkan warning bahwa argumen offset dan fi tidak digunakan dalam fungsi ini. (void) digunakan untuk menandakan bahwa argumen tersebut tidak digunakan dan menghindari warning dari kompilator.

- `DIR *dir` dan `struct dirent *entry`.  Deklarasi variabel dir yang bertipe DIR* dan entry yang bertipe struct dirent*. dir digunakan untuk menyimpan pointer ke direktori yang akan dibaca, sedangkan entry digunakan untuk menyimpan pointer ke entri-entri di dalam direktori.

- `dir = opendir(path)`. Kita membuka direktori yang akan dibaca menggunakan fungsi opendir. Jika direktori tidak dapat dibuka, kita mengembalikan kode error menggunakan -errno.

- `while ((entry = readdir(dir)) != NULL) { ... }`. Kita membaca setiap entri di dalam direktori yang sedang dibaca menggunakan fungsi readdir. Selama masih terdapat entri yang belum dibaca, kita akan melakukan iterasi.

- `struct stat st; memset(&st, 0, sizeof(st))`. Kita membuat variabel st yang bertipe struct stat untuk menyimpan informasi status dari setiap entri di dalam direktori. memset digunakan untuk menginisialisasi st dengan nilai 0 atau NULL.

- `st.st_ino = entry->d_ino`. Kita mengisi st.st_ino dengan nilai d_ino dari entri saat ini, yang merupakan nomor inode.

- `st.st_mode = entry->d_type << 12`. Kita mengisi st.st_mode dengan nilai d_type dari entri saat ini yang telah digeser (<<) sebanyak 12 bit. Hal ini dilakukan untuk mendapatkan informasi mode permissions dari entri.

- `if (filler(buf, entry->d_name, &st, 0) != 0) { break; }`. Kita memanggil fungsi filler untuk mengisi data entri ke dalam buffer buf. Fungsi filler mengambil argumen buf sebagai buffer, entry->d_name sebagai nama entri, &st sebagai pointer ke struktur status st, dan 0 sebagai flags. Jika fungsi filler mengembalikan nilai bukan 0, berarti buffer sudah penuh dan kita harus menghentikan iterasi.

- `closedir(dir)`. Setelah selesai membaca semua entri di dalam direktori, kita menutup direktori menggunakan fungsi closedir.

- `return 0`. Fungsi read_directory mengembalikan nilai 0. Nilai kembalian ini menunjukkan bahwa operasi telah berhasil.

**Fungsi fuse_operations**
```
static struct fuse_operations operations = {
    .mkdir = create_directory,
    .rmdir = log_rmdir,
    .unlink = log_unlink,
    .readdir = read_directory,
};
```
- `static struct fuse_operations operations = { ... }`. Ini adalah deklarasi variabel operations yang bertipe struct fuse_operations. Variabel ini digunakan untuk menyimpan daftar fungsi callback yang akan dipanggil oleh sistem FUSE saat terjadi operasi- operasi tertentu pada filesystem yang sedang dibuat.

- `.mkdir = create_directory`. Ini adalah pengaturan callback untuk operasi pembuatan direktori (mkdir). Ketika ada permintaan pembuatan direktori, fungsi create_directory akan dipanggil.

- `.rmdir = log_rmdir`. Ini adalah pengaturan callback untuk operasi penghapusan direktori (rmdir). Ketika ada permintaan penghapusan direktori, fungsi log_rmdir akan dipanggil.

- `.unlink = log_unlink`. Ini adalah pengaturan callback untuk operasi penghapusan file (unlink). Ketika ada permintaan penghapusan file, fungsi log_unlink akan dipanggil.

- `.readdir = read_directory`. Ini adalah pengaturan callback untuk operasi membaca isi direktori (readdir). Ketika ada permintaan membaca isi direktori, fungsi read_directory akan dipanggil.

**Fungsi main**
```
int main(int argc, char *argv[]) {
    
    if (access("/home/aida", W_OK) != 0) {
    fprintf(stderr, "Tidak dapat mengakses direktori /home/aida.\n");
    return -1;
    }

    // Inisialisasi fuse
    umask(0);
    return fuse_main(argc, argv, &operations, NULL);
}
```
- `int main(int argc, char *argv[]) { ... }`. Ini adalah deklarasi fungsi main dengan parameter argc dan argv. argc adalah jumlah argumen yang diberikan pada program saat dijalankan, sedangkan argv adalah array yang berisi argumen-argumen tersebut.

- `if (access("/home/aida", W_OK) != 0) { ... }`. Pada bagian ini, dilakukan pemeriksaan apakah program memiliki akses tulis (W_OK) ke direktori /home/aida. Jika akses tidak tersedia, maka akan dicetak pesan kesalahan dan program akan mengembalikan nilai -1, menandakan adanya kesalahan.

- `umask(0)`. Fungsi umask digunakan untuk mengatur nilai umask menjadi 0, yang mengizinkan semua hak akses pada file dan direktori yang dibuat oleh program. Dalam konteks ini, nilai umask diatur menjadi 0 untuk memastikan bahwa hak akses file dan direktori yang dibuat sesuai dengan yang diinginkan.

- `return fuse_main(argc, argv, &operations, NULL)`. Pemanggilan fungsi fuse_main digunakan untuk menjalankan filesystem menggunakan FUSE. Fungsi ini menerima argumen argc dan argv dari fungsi main, serta parameter &operations yang merupakan pointer ke struktur fuse_operations yang berisi daftar fungsi callback yang akan dipanggil saat operasi-operasi tertentu terjadi pada filesystem yang dibuat. Argumen terakhir, NULL, digunakan untuk menyediakan data tambahan jika diperlukan oleh fungsi callback, tetapi dalam kasus ini tidak digunakan.

**Masalah**

Saat saya menjalankan code diatas menggunakan 
```
gcc -Wall modular.c -o modular `pkg-config fuse --cflags --libs`
```
lalu membuat direktori untuk mount menggunakan `mkdir BSI`
lalu di jalankan menggunakan `./modular BSI`

Code diatas bisa dijalankan, namun file log yang harusnya berada pada path `/home/aida/` tidak terbuat

Jadi tidak bisa melanjutkan atau mengetes code nya agar isi file log bisa dilihat benar atau salahnya formatnya

## Soal Nomor 5

**a) Unduh file rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.**

Menggunakan fungsi download_file() untuk mendownload rahasia.zip
```
static int download_file() {
    char url[] = "https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes";
    char command[256];
    sprintf(command, "wget -O rahasia.zip '%s'", url);
    int status = system(command);
    if (status == -1) {
        printf("Gagal menjalankan perintah wget.\n");
        return 1;
    }
    printf("File berhasil diunduh.\n");
    return 0;
}
```
- Menggabungkan command linux dengan url menggunakan sprintf dan disimpan ke command.
- Kemudian command yang berisi command download file dieksekusi menggunakan system().

```
    if (access("rahasia.zip", F_OK) != 0) {
        download_file();
        system("unzip rahasia.zip");
    }
```
- Jika rahasia.zip belum ada / belum didownload, maka akan dipanggil fungsi download_file() dan setelah download, rahasia.zip diunzip menggunakan system(unzip).

**b)Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_<Kode Kelompok> pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile.**

docker-compose.yml
```
version: '3'
services:
  myapp:
    image: rahasia_di_docker_f06
    container_name: rahasia_docker
    command: tail -f /dev/null
    volumes:
      - /home/plankton/Modul4/rahasia:/usr/share
```
- Untuk mount folder rahasia ke docker, harus dibuat containernya terlebih dahulu.
- Menggunakan docker-compose, dengan nama image = rahasia_di_docker_f06 sesuai dengan format di soal, container_name = rahasia_docker.
- Volumes = /home/plankton/Modul4/rahasia:/usr/share, dimana /home/plankton/Modul4/rahasia adalah path direktori yang ingin di mount dan /usr/share adalah path tujuan mount di docker.

Setelah membuat docker-compose, saatnya pull image dengan base image ubuntu docal fossa dan up docker-composenya.

rahasia.c
```
        system("docker pull ubuntu:focal");
        system("docker-compose up -d");
```

**c)Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5.**

ada 3 fungsi yang diterapkan dalam poin ini.

1. adausername(char* username)
Fungsi ini dibuat untuk cek apakah username yang diinputkan oleh user sudah pernah diregister atau belum.

```
    FILE * file = fopen("regis.txt", "r");
    if (file == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }
    char line[202];
    while (fgets(line, sizeof(line), file) != NULL) {
        char * token = strtok(line, ";");
        if (token != NULL && strcmp(token, username) == 0) {
            fclose(file);
            return 1;
        }
    }
    fclose(file);
    return 0;
```
- Pertama membaca regis.txt per baris menggunakan fgets.
- Kemudian username yang berada di regis.txt diambil menggunakan strtok(line, ";") yang artinya akan dibaca dan diambil string sampai ';' saja, string setelah ';' diabaikan.
- Username tadi kemudian dicocokkan dengan username inputan user.
- Jika username yang diinputkan user sama dengan username yang ada di regis.txt yang berarti username sudah pernah diregister. akan return 1. Begitu juga sebaliknya.

2. md5enkrip(const char * password, char * encryptedPassword)

Fungsi ini untuk melakukan enkripsi password yang diinputkan oleh user menggunakan md5.

```
    char command[100];
    sprintf(command, "echo -n %s | md5sum | awk '{print $1}'", password);
    FILE * pipe = popen(command, "r");
    if (pipe == NULL) {
        printf("Error saat mengenkripsi password.\n");
        exit(1);
    }
    fgets(encryptedPassword, 100, pipe);
    // Menghapus karakter newline yang dihasilkan oleh md5sum
    encryptedPassword[strcspn(encryptedPassword, "\n")] = '\0';
    pclose(pipe);
```
- Sprintf untuk menggabungkan command. Commandnya adalah md5sum untuk mengenkripsi password menggunakan md5, kemudian awk print untuk print hasilnya.
- Command diread menggunakan popen dan diambil hasil enkripsinya menggunakan fgets.

3. regist(const char * username, const char * password)

Fungsi ini berguna untuk mencatat username user dan password yang terlah dienkripsi tadi ke file regis.txt

```
    FILE * file = fopen("regis.txt", "a");
    if (file == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }
    fprintf(file, "%s;%s\n", username, password);
    fclose(file);
    printf("Registrasi berhasil.\n");
    return 0;
```
- Open regis.txt menggunakan "a" atau append.
- Kemudian fprintf untuk appen text username dan password. dengan format username;password.

**d) Folder tersebut hanya dapat diakses oleh user yang telah melakukan login. User yang login dapat membaca folder dan file yang di-mount kemudian dengan menggunakan FUSE lakukan rename pada folder menjadi <Nama_Folder>_<Kode_Kelompok> dan <Kode_Kelompok>_<Nama_File>.<ext>.**

1. Buat system login 
```
    const char * password) {
    FILE * file = fopen("regis.txt", "r");
    if (file == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }
    char line[100];
    while (fgets(line, sizeof(line), file) != NULL) {
        char * token = strtok(line, ";");
        if (token != NULL && strcmp(token, username) == 0) {
            char * savedPassword = strtok(NULL, ";");
            savedPassword[strcspn(savedPassword, "\n")] = '\0';
            if (savedPassword != NULL && strcmp(savedPassword, password) == 0) {
                fclose(file);
                return 1; // Berhasil login
            } else {
                fclose(file);
                return 0; // Password salah
            }
        }
    }
    fclose(file);
    return -1; // Username tidak ditemukan
```
- Membaca regis.txt per baris dari line 1 sampai akhir menggunakan fgets.
- Cek apakah username tersedia atau tidak menggunakan strtok dan strcmp.
- Jika tersedia, lanjut cek validasi password. Dengan cara mengambil password yang ada di regis.txt lalu mencocokkannya dengan password inputan user.

2. Ketika login berhasil, user dapat read isi /usr/share
```
            if (loginResult == 1) {
                printf("Login berhasil.\n");
                while (1) {
                    printf("Ingin read isi directory? (Y/N)\n");
                    scanf(" %c", & tanya);
                    if (tanya == 'Y') {
                        system("docker exec -it rahasia_docker /bin/bash -c 'ls /usr/share'");
                    } else {
                        break;
                    }
                }
```
- Sistem akan bertanya pakah ingin read atau tidak, Jika 'Y' maka akan read direktori dan jika 'N' akan keluar dari sistem.
- Untuk membaca direktori, menggunakan command docker exec -it rahasia_docker /bin/bash -c 'ls /usr/share' dimana "ls /usr/share" seperti list pada linux command.

3. Rename isi dari direktori
Untuk rename isi direktori ada di awal ketika return fuse_main. Rename isi dari direktori memakai 2 fuse operation yaitu rename dan readdir.

**e)List seluruh folder, subfolder, dan file yang telah di-rename dalam file result.txt menggunakan tree kemudian hitung file tersebut berdasarkan extension dan output-kan menjadi extension.txt.**

Untuk solusi ini saya gabungkan menjadi 1 fungsi yaitu fungsi menghitung().

1. List seluruh folder, subfolder, dan file yang telah direname dalam file result.txt menggunakan tree.
```
    system("touch result.txt");
    system("touch extension.txt");
    system("tree rahasia > result.txt");
```
- Disini dibuat dulu file txt nya.
- Kemudian untuk list dengan tree dapat menggunakan command "tree rahasia" dimana rahasia adalah direktori yang ingin di list dan "> result.txt" adalah untuk append output treenya ke file result.txt

2. hitung file tersebut berdasarkan extension dan output-kan menjadi extension.txt

```
    resultFile = fopen("result.txt", "r");
    if (resultFile == NULL) {
        printf("Gagal membuka file result.txt.\n");
        return 1;
    }
    // Membaca setiap baris dari file result.txt
    while (fgets(line, sizeof(line), resultFile) != NULL) {
        // Menghapus karakter newline di akhir baris
        line[strcspn(line, "\n")] = '\0';
        // Memeriksa ekstensi dari line
        char * extension = strrchr(line, '.');
        char * folderCheck = strstr(line, "_F06");
        if (folderCheck != NULL) {
            folderCount++;
        } else if (extension != NULL) {
            // Memeriksa ekstensi yang valid
            if (strcmp(extension, ".jpg") == 0) {
                jpgCount++;
            } else if (strcmp(extension, ".png") == 0) {
                pngCount++;
            ....
            }
        }
    }
```
- Akan dibaca per line dari awal hingga habis pada file result.txt.
- Kemudian akan divalidasi apakah dalam line tersebut mengandung _F06 atau tidak, dimana jika mengandung "_F06" berarti direktori.
- Jika benar maka folderCount akan bertambah 1. Jika tidak, maka akan dicek lagi apakah extensionnya sesuai atau tidak. seperti apakah extensionnya .jpg atau tidak, jika iya maka jpgCountnya akan bertambah. dsb.

```
    extensionFile = fopen("extension.txt", "w");
    if (extensionFile == NULL) {
        printf("Gagal membuka file extension.txt.\n");
        return 1;
    }
    // Menulis jumlah folder dan file dengan ekstensi yang valid ke dalam file extension.txt
    fprintf(extensionFile, "folder = %d\n", folderCount);
    fprintf(extensionFile, "jpg = %d\n", jpgCount);
    ....
```
- Buka extension.txt untuk diwrite.
- Terakhir, fprintf untuk menulis hasil dari perhitungan ke extension.txt dengan format seperti di soal.
